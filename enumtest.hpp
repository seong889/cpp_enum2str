#ifndef ENUMTEST_HPP
#define ENUMTEST_HPP
#include <QMap>
#include <QString>

#define __INCLUDE__(hash, name) hash##include #name

#define MACRO(hash, name, cont)

//enum class test : int {
//    #define X(a) a,
//    #define X2(a, b)

//    #include "test.enum"
//    #undef X
//    #undef X2
//};

//const QMap<test, QString> enum2str_test = {
//    #define X(a) {test::a, QString(#a)},
//    #define X2(a, b)
//    #include "test.enum"
//    #undef X
//    #undef X2
//};

//const QMap<QString, test> str2enum_test = {
//    #define X(a) {QString(#a), test::a},
//    #define X2(a, b) {QString(#a), test::b},
//    #include "test.enum"
//    #undef X
//    #undef X2
//};

#define __E_TYPE__ test
#define __E_NAME__ "test.enum"
#include "enum.def"
#undef __E_TYPE__
#undef __E_NAME__

#define __E_TYPE__ t
#define __E_NAME__ "test.enum"
#include "enum.def"
#undef __E_TYPE__
#undef __E_NAME__

#endif // ENUMTEST_HPP
