#include <QCoreApplication>
#include "enumtest.hpp"
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    qInfo() << int(test::STAND) << enum2str_test[test::STAND];
    qInfo() << int(str2enum_test["STD"]) << int(str2enum_test["STAND"]);


    qInfo() << int(t::STAND) << enum2str_t[t::STAND];
    qInfo() << int(str2enum_t["STD"]) << int(str2enum_t["STAND1"]);

    return a.exec();
}
